# Bloc
## Zero knowledge, E2EE and resilient cloud file storage

## Table of contents
- [Introduction](#introduction)
- [Roadmap](#roadmap)
  - [Beta](#beta)
  - [Realase](#release)
- [Security](#security)
  - [Anonymity](#anonymity)
  - [Users authentification](#users-authentification)
  - [File encryption](#file-encryption)
- [Deploy](#deploy)
  - [Docker Compose](#docker-compose)
- [Contribute](#contribute)
  - [Quick start](#quick-start-1)
  - [Backend](#backend)
  - [Frontend](#frontend)
- [License](#license)

## Introduction

Bloc is a cloud storage service made for peoples who actually need a high security level.
This is made possible by encrypting files directly on the client side and authenticating them with a unique public key. No email, no password, no personal datas, just a public key for encrypting files and proving that you own your files.

On the middle term we would like to make bloc a decentralized storage network allowing it to be more resilient and resistant toward servers shutdown, police raid, etc.

## Roadmap
The labels we use (Beta, Release) are the states of the project until the aboves goals / features are ready.

So actually we are in Alpha version but whe, the Beta will be ready we will publish our public instance for getting users feedbacks to work on the release.

### Beta
- [x] Filesystem storage
- [ ] S3 object storage
- [x] Public key authentication (liboyd)
- [x] Client side encryption (liboxyd)
- [ ] Full metadata encryption (liboxyd)
- [ ] Public file sharing (link)
- [ ] Private file sharing (user to user)
- [ ] VueJS web frontend

### Release
- [ ] Desktop / Mobile app using tauri for native liboxyd integration (safer than using web browser's localstorage)
- [ ] Collaborative file editor
- [ ] Polar / IPFS / Decentralized storage network
- [ ] Oauth2 integration (need to figure out how to do it)

## Security
Our threat model is simple: if a Bloc instance is hacked, if it come from a bug in the backend, a breach in the server's configuration or if the server is seized by authorities, nothing can be used by the attackers to localize the users, read the files, etc.
A Bloc instance shouldn't need to be trusted to be used for storing files, only the client must be trusted, this is why it's better to self-host your own frontend, use a native client or a trusted provider.

### Anonymity
To first allow users to stay anonymous we first never ask or need any personal data, only a public key is required for verifying signatures, so for authenticating the user and proof that they own their files.

### Users authentification
For verifying a user, a signed challenge must be sent for every requests, to avoid replay attacks, the challenge is stored on a persistant database to avoid slowing down the disks, the last 50 challenges are loaded in memory.

### File encryption
Each users have a unique [secp256k1](https://en.bitcoin.it/wiki/Secp256k1) keypair, a random 256 bits [XChaCha20-Poy1305](https://en.wikipedia.org/wiki/ChaCha20-Poly1305) key is generated for each files when uploading them, the file key is encrypted using user's public key with the [ECIES algorithm](https://en.wikipedia.org/wiki/Integrated_Encryption_Scheme) and then is sent to the server to store it. Every single cryptographic operations are made on the client side using liboxyd rust / wasm implementation. liboxyd only use officials and audited dependancies for all the encryption algorithms.

## Deploy
### Docker compose
```sh
$ git clone https://git.coldwire.org/coldwire/bloc.git
$ cd bloc
(bloc) $ docker-compose up -d # Build and run bloc
```

## Contribute
### Quick start
```sh
$ git clone https://git.coldwire.org/coldwire/bloc.git
$ cd bloc
(bloc) $ make setup # install modules dependancies for front and back
(bloc) $ make run # run frontend and backend
```

### Backend
```sh
(bloc) $ make setup back # install modules and set dev config for backend
(bloc) $ make run back # run backend server
```

### Frontend
```sh
(bloc) $ make setup front # install modules for frontend
(bloc) $ make run front # run frontend dev server
(bloc) $ make build front # build frontend as a static website
```

## License
You can find the license [here](LICENSE.md)